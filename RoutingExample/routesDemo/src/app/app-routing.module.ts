import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookComponent } from './books/book.component';
import { BookDetailsComponent } from './books/book-details/book-details.component';


const routes: Routes = [
  {
    path: 'bookList', component: BookComponent
  },
  {
    path: 'book/:id', component  : BookDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
