import { Component, OnInit } from '@angular/core';
import { BooksService } from '../common/books.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  books: any[];
  errorMessage: string;
  bookId: string;
  chosenBook: any;

  constructor(private bookService: BooksService, private route: ActivatedRoute) { }

  getBooks() {
    this.bookService.getAllBooks().subscribe(
      ibooks => {
        this.books = ibooks;
        console.log(this.books);
        this.route.params.subscribe(param => this.bookId = param['id']);
        console.log(this.bookId);
        this.getBookDetails(this.bookId);
      },
      error => console.log(error));
  }

  getBookDetails(bookId) {
    console.log(bookId);
    for (const iterator of this.books) {
      if (iterator.id == bookId) {
        this.chosenBook = iterator;
      }
    }
  }
  ngOnInit() {
    this.getBooks();
  }

}
