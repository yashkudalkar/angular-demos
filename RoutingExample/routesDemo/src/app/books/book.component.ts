import { Component, OnInit } from '@angular/core';
import { BooksService } from './common/books.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  constructor(private bookService: BooksService) { }

  books: any [];
  ngOnInit(): void {
    this.getAllBooks();
  }

  getAllBooks(){
  this.bookService.getAllBooks().subscribe(
    ibooks => {
      this.books = ibooks;
    }
  );
  }

}
